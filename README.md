### Apex Specialist

[Link to Challenge](https://trailhead.salesforce.com/en/content/learn/superbadges/superbadge_apex)

- What You'll Be Doing to Earn This Superbadge
  - Automate record creation using Apex triggers
  - Synchronize Salesforce data with an external system using asynchronous REST callouts
  - Schedule synchronization using Apex code
  - Test automation logic to confirm Apex trigger side effects
  - Test integration logic using callout mocks
  - Test scheduling logic to confirm action gets queued
  
- Concepts Tested in This Superbadge
  - Apex Triggers
  - Asynchronous Apex
  - Apex Integration
  - Apex Testing

- Pre-work and Notes

  - Grab a pen and paper. You may want to jot down notes as you read the requirements.


Refer to the Apex Specialist Superbadge: Trailhead Challenge Help document for detailed resources and documentation.

Use the naming conventions specified in the requirements document to ensure a successful deployment.

Review the data schema in your modified Salesforce org as you read the detailed requirements below.
