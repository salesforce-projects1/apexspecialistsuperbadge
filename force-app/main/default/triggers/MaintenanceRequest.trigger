trigger MaintenanceRequest on Case (after update) {
    
    List<Case> closedCaseList = new List<Case>();
    
    for(Case c : Trigger.new){
    	if (c.IsClosed && (c.Type.equals('Repair') || c.Type.equals('Routine Maintenance'))){
        	closedCaseList.add(c);
           
        }
    }
     // ToDo: Call MaintenanceRequestHelper.updateWorkOrders
    if (closedCaseList.size() != 0) {
     	System.debug('Trigger Fired');
     	MaintenanceRequestHelper.updateWorkOrders(closedCaseList);    
	} 
    
       
}