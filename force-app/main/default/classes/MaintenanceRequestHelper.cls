public with sharing class MaintenanceRequestHelper {
    
    public static void updateWorkOrders(List<Case> closedCaseList) {
        
        List<Equipment_Maintenance_Item__c> mainItems = [SELECT Id, Equipment__c, Maintenance_Request__c FROM Equipment_Maintenance_Item__c Where Equipment__c != null Limit 200];
        
        List<Equipment_Maintenance_Item__c> mainItemsInCaseList = new List<Equipment_Maintenance_Item__c>();
        
        List<Case> followUpCaseList = new List<Case>();
        
        List<Product2> product2List = [Select Id, Maintenance_Cycle__c From Product2 Where Maintenance_Cycle__c != null Limit 200];       
       	 	                            
        Map<Id, Decimal> cycleTimes = new Map<Id, Decimal>();
        
        List<Decimal> cycles = new List<Decimal>();  
        
        for (Equipment_Maintenance_Item__c item : mainItems) {
            
            for (Product2 p2 : product2List) {
                if (item.Equipment__c == p2.Id) {
                    cycles.add(p2.Maintenance_Cycle__c);
                }
        	}
            
            for (Case c : closedCaseList) {
                if (c.id == item.Maintenance_Request__c) {
                    mainItemsInCaseList.add(item);
                }
            }
            cycles.sort();
            cycleTimes.put(item.Maintenance_Request__c, cycles[0]);
        	}
            
           
        for (Case c : closedCaseList) {
            
       		Date dueDate;  
            
            if (cycleTimes.get(c.Id) != null) {
               	dueDate = system.today().addDays((Integer)cycleTimes.get(c.Id));
            } else {
                 dueDate = system.today(); 
            }
            
            followUpCaseList.add(new Case(
                              Subject = 'Scheduled Maintenance',
                              Type='Routine Maintenance',
                			  Status ='New',
                              Vehicle__c=c.Vehicle__c,
                              Product__c=c.Product__c,
                              Date_Reported__c= system.today(),
                          	  Date_Due__c = dueDate));
        }
        System.debug(followUpCaseList);
        if (followUpCaseList.size() > 0) {
        	insert followUpCaseList;
            System.debug('DML Operation insert');
    	}
        
        Map<Id, Case> newCaseMap = new Map<Id, Case>();
        for (Case c : followUpCaseList) {
            newCaseMap.put(c.Id, c);
            System.debug(c.Id);
        }
        
        List<Equipment_Maintenance_Item__c> newEmiList = new List<Equipment_Maintenance_Item__c>();
        System.debug(newCaseMap);
        
       	//List<Case> newCaseList = [SELECT Id, Product__c, Vehicle__c FROM Case
       //              				WHERE Type = 'Routine Maintenance' AND
        //              				Status = 'New' AND Date_Reported__c = :system.today()];
        
        for (Case c : newCaseMap.values()) {
        
        	for (Equipment_Maintenance_Item__c item : mainItemsInCaseList) {
           			newEmiList.add(new Equipment_Maintenance_Item__c(
                		Equipment__c = item.Equipment__c,
                		Maintenance_Request__c = c.Id
                	));
                }
   	     	
        }
           
        if (newEmiList.size() > 0) {
             insert newEmiList;
            System.debug('Emi list inserted');
            }  
   	

   }        
    
}